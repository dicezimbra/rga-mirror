
package com.cezimbra.projeto.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Contact extends RealmObject implements Serializable {



    @PrimaryKey
    private int id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("born")
    @Expose
    private String born;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("photo")
    @Expose
    private String photo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The born
     */
    public String getBorn() {
        return born;
    }

    /**
     * 
     * @param born
     *     The born
     */
    public void setBorn(String born) {
        this.born = born;
    }

    /**
     * 
     * @return
     *     The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * 
     * @param bio
     *     The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * 
     * @return
     *     The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 
     * @param photo
     *     The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public enum CONTACTS{
        ID,
        NAME,
        EMAIL,
        BORN,
        BIO,
        PHOTO
    }

    @Override
    public String toString() {

        JSONObject obj = new JSONObject();
        try {

            obj.put(String.valueOf(CONTACTS.ID), String.valueOf(this.id));
            obj.put(String.valueOf(CONTACTS.NAME), this.name);
            obj.put(String.valueOf(CONTACTS.EMAIL), this.email);
            obj.put(String.valueOf(CONTACTS.BORN), this.born);
            obj.put(String.valueOf(CONTACTS.BIO), this.bio);
            obj.put(String.valueOf(CONTACTS.PHOTO), this.photo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj.toString();
    }

    public static Contact toObject(String sContact){

        Contact contact = new Contact();
        try {
            JSONObject obj = new JSONObject(sContact);
            contact.setId(Integer.parseInt(obj.getString(String.valueOf(CONTACTS.ID))));
            contact.setName(obj.getString(String.valueOf(CONTACTS.NAME)));
            contact.setEmail(obj.getString(String.valueOf(CONTACTS.EMAIL)));
            contact.setBorn(obj.getString(String.valueOf(CONTACTS.BORN)));
            contact.setBio(obj.getString(String.valueOf(CONTACTS.BIO)));
            contact.setPhoto(obj.getString(String.valueOf(CONTACTS.PHOTO)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contact;
    }

}
