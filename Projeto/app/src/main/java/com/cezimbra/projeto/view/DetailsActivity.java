package com.cezimbra.projeto.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.cezimbra.projeto.R;
import com.cezimbra.projeto.controller.ApplicationComponent;
import com.cezimbra.projeto.interfaces.ContactBO;
import com.cezimbra.projeto.model.Contact;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

public class DetailsActivity extends BaseActivity {

    public static final String CONTACT = "contact";

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.email)
    TextView email;

    @InjectView(R.id.born)
    TextView born;

    @InjectView(R.id.bio)
    TextView bio;

    @InjectView(R.id.progress)
    fr.castorflex.android.circularprogressbar.CircularProgressBar progress;

    @InjectView(R.id.image)
    com.makeramen.roundedimageview.RoundedImageView image;

    @Inject
    ContactBO mContactBO;

    Contact mContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setupToolbar();
        injectViews();

        Intent it = getIntent();
        Bundle bundle = it.getExtras();
        if (bundle == null) {
            return;
        }

        String sContactInfo = bundle.getString(CONTACT);
        mContact = Contact.toObject(sContactInfo);

        setupViews();
    }

    private void setupViews() {

        name.setText(mContact.getName());
        email.setText(mContact.getEmail());
        born.setText(mContact.getBorn());
        bio.setText(mContact.getBio());

        if(mContact.getPhoto() == null || mContact.getPhoto().trim().length() == 0){
            image.setImageResource(R.drawable.nopicture);
            progress.setVisibility(View.GONE);
        }else {

            Picasso.with(this)

                    .load(mContact.getPhoto())
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            image.setImageResource(R.drawable.nopicture);
                            progress.setVisibility(View.GONE);
                        }
                    });
        }
    }

    @Override
    protected void performInjection(ApplicationComponent component) {
        component.inject(this);
    }


    @OnClick(R.id.button_delete)
    public void DeleteButtonClick(){
        mContactBO.deleteContact(mContact.getId());
        this.finish();
        ContactsActivity.mRealChangeInterface.onRealmChanges();
    }

    @OnClick(R.id.button_edit)
    public void EditButtonClick(){
        Intent it = new Intent(this, EditActivity.class);
        it.putExtra(CONTACT, mContact.toString());
        startActivity(it);
        this.finish();
    }
}
