package com.cezimbra.projeto.controller;

import com.cezimbra.projeto.BO.ContactBOImp;
import com.cezimbra.projeto.controller.ApplicationModule;
import com.cezimbra.projeto.view.AddContactActivity;
import com.cezimbra.projeto.view.ContactsActivity;
import com.cezimbra.projeto.view.DetailsActivity;
import com.cezimbra.projeto.view.EditActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by cezimbra on 31/08/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ContactsActivity activity);
    void inject(DetailsActivity activity);
    void inject(AddContactActivity activity);
    void inject(ContactBOImp activity);

    void inject(EditActivity editActivity);
}
