package com.cezimbra.projeto.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.cezimbra.projeto.R;
import com.cezimbra.projeto.model.Contact;
import com.cezimbra.projeto.view.DetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by cezimbra on 31/08/2016.
 */

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Contact> mContacts;


    public ContactsAdapter(Context context, ArrayList<Contact> contacts) {
        this.mContext = context;
        this.mContacts = contacts;
    }

    private void bindDefaultFeedItem(int position,final ContactHolder holder) {

        try {
            final Contact contact = mContacts.get(position);

            holder.name.setText(contact.getName());
            holder.description.setText(contact.getEmail());

            final View progress = holder.progress;

            if(contact.getPhoto() == null || contact.getPhoto().trim().length() == 0){
                holder.picture.setImageResource(R.drawable.nopicture);
                progress.setVisibility(View.GONE);
            }else {

                Picasso.with(mContext)

                        .load(contact.getPhoto())
                        .into(holder.picture, new Callback() {
                            @Override
                            public void onSuccess() {
                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                holder.picture.setImageResource(R.drawable.nopicture);
                                progress.setVisibility(View.GONE);
                            }
                        });
            }
            holder.click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(mContext, DetailsActivity.class);
                    it.putExtra(DetailsActivity.CONTACT, contact.toString());
                    mContext.startActivity(it);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_contact, parent, false);
        final ContactHolder cellFeedViewHolder = new ContactHolder(view);
        return cellFeedViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        bindDefaultFeedItem(position, (ContactHolder) viewHolder);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public static class ContactHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.description)
        TextView description;

        @InjectView(R.id.picture)
        ImageView picture;

        @InjectView(R.id.click)
        View click;

        @InjectView(R.id.progress)
        fr.castorflex.android.circularprogressbar.CircularProgressBar progress;

        public ContactHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
