package com.cezimbra.projeto.view;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.TextView;
import com.cezimbra.projeto.interfaces.ContactBO;
import com.cezimbra.projeto.R;
import com.cezimbra.projeto.controller.ApplicationComponent;

import javax.inject.Inject;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddContactActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        setupToolbar();
        injectViews();

        inputLayoutName.setErrorEnabled(true);
        inputLayoutBio.setErrorEnabled(true);
        inputLayoutEmail.setErrorEnabled(true);
        inputLayoutBorn.setErrorEnabled(true);

        textButtonOk.setText("ADICIONAR");
    }

    @OnClick(R.id.button_ok)
    public void buttonOkClick(){

        String name = inputName.getText().toString();
        String email = inputEmail.getText().toString();
        String bio = inputBio.getText().toString();
        String born = inputBorn.getText().toString();

        if(name.trim().length() == 0){
            inputLayoutName.setError("Entre com um nome");
            return;
        }else{
            inputLayoutName.setError("");
        }

        if(email.trim().length() == 0 || !email.contains("@") || !email.contains(".") ){
            inputLayoutEmail.setError("Entre com um email");
            return;
        }else{
            inputLayoutEmail.setError("");
        }

        if(bio.trim().length() == 0){
            inputLayoutBio.setError("Entre com uma bio");
            return;
        }else{
            inputLayoutBio.setError("");
        }

        if(born.trim().length() == 0 || !born.trim().contains("/")){
            inputLayoutBorn.setError("Tente com DD/MM/YYYY");
            return;
        }else{
            inputLayoutBorn.setError("");
        }

        mContactBO.insertContact(name, email, born, bio);
        ContactsActivity.mRealChangeInterface.onRealmChanges();
        this.finish();
    }

    @Override
    protected void performInjection(ApplicationComponent component) {
        component.inject(this);
    }

    @Inject
    ContactBO mContactBO;

    @InjectView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;

    @InjectView(R.id.input_layout_bio)
    TextInputLayout inputLayoutBio;

    @InjectView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;

    @InjectView(R.id.input_layout_born)
    TextInputLayout inputLayoutBorn;

    @InjectView(R.id.text_button_ok)
    TextView textButtonOk;

    @InjectView(R.id.input_name)
    EditText inputName;

    @InjectView(R.id.input_bio)
    EditText inputBio;

    @InjectView(R.id.input_email)
    EditText inputEmail;

    @InjectView(R.id.input_born)
    EditText inputBorn;
}
