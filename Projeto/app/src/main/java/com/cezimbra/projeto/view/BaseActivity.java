package com.cezimbra.projeto.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.cezimbra.projeto.R;
import com.cezimbra.projeto.controller.ApplicationComponent;
import com.cezimbra.projeto.controller.ApplicationController;

import butterknife.ButterKnife;

/**
 * Created by cezimbra on 31/08/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setupToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public void injectViews(){
        ButterKnife.inject(this);
        performInjection(ApplicationController.from(this).getComponent());
    }

    public ApplicationController getApp(){
        return (ApplicationController) getApplication();
    }

    protected abstract void performInjection(ApplicationComponent component);


}
