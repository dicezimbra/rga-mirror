package com.cezimbra.projeto.BO;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.cezimbra.projeto.interfaces.ContactBO;
import com.cezimbra.projeto.model.Contact;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by cezimbra on 31/08/2016.
 */
public class ContactBOImp implements ContactBO {

    Realm mRealm;
    Context mContext;
    public ContactBOImp(Realm mRealm, Context context) {
        this.mRealm = mRealm;
        this.mContext = context;
    }

    @Override
    public void insertContact(String name, String email, String born, String bio) {
        mRealm.beginTransaction();

        Contact contact = mRealm.createObject(Contact.class);
        contact.setName(name);
        contact.setBio(bio);
        contact.setId(getNextId());
        contact.setBorn(born);
        contact.setEmail(email);
        mRealm.commitTransaction();
    }

    @Override
    public void deleteContact(int id) {
        mRealm.beginTransaction();
        Contact contact = mRealm.where(Contact.class).equalTo("id", id).findFirst();
        contact.deleteFromRealm();
        mRealm.commitTransaction();
    }

    @Override
    public void updateContact(int id, String name, String email, String born, String bio) {

        mRealm.beginTransaction();
        Contact contact = mRealm.where(Contact.class).equalTo("id", id).findFirst();
        contact.setName(name);
        contact.setEmail(email);
        contact.setBio(bio);
        contact.setBorn(born);
        mRealm.commitTransaction();
    }

    @Override
    public void updateContact(Contact contact) {
        mRealm.beginTransaction();
        Contact contactlocal = mRealm.where(Contact.class).equalTo("id", contact.getId()).findFirst();
        contactlocal.setName(contact.getName());
        contactlocal.setEmail(contact.getEmail());
        contactlocal.setBio(contact.getBio());
        contactlocal.setBorn(contact.getBorn());
        mRealm.commitTransaction();
    }

    @Override
    public List<Contact> getContacts() {

        RealmQuery query = mRealm.where(Contact.class);
        RealmResults results = query.findAllSorted("id");

        return results;
    }

    @Override
    public void addAll(List<Contact> all) {
        mRealm.beginTransaction();

        List<Contact> contacts = new ArrayList<>();

        for (int i = 0; i < all.size(); i++) {
            Contact contact = mRealm.createObject(Contact.class);
            contact.setBorn(all.get(i).getBorn());
            contact.setEmail(all.get(i).getEmail());
            contact.setBio(all.get(i).getBio());
            contact.setName(all.get(i).getName());
            contact.setPhoto(all.get(i).getPhoto());
            contact.setId(getNextId());
            contacts.add(contact);
        }

        mRealm.copyToRealm(contacts);
        mRealm.commitTransaction();
    }

    private int getNextId(){
        SharedPreferences prefs = mContext.getSharedPreferences("bd", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        int id = prefs.getInt("nextId", 1);
        edit.putInt("nextId", id+1);
        edit.commit();
        return id;
    }


}
