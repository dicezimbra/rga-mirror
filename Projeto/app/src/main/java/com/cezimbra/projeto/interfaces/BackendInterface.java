package com.cezimbra.projeto.interfaces;

import com.cezimbra.projeto.model.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by cezimbra on 31/08/2016.
 */

public interface BackendInterface {
    @GET("rgasp-mobile-test/v1/content.json")
    Call<List<Contact>> getContacts();
}
