package com.cezimbra.projeto.interfaces;

import com.cezimbra.projeto.model.Contact;

import java.util.List;

/**
 * Created by cezimbra on 31/08/2016.
 */

public interface ContactBO
{
    public void insertContact( String name, String email, String born, String bio);
    public void deleteContact(int id);
    public void updateContact(int id, String name, String email, String born, String bio);
    public void updateContact(Contact contact);
    public List<Contact> getContacts();
    public void addAll(List<Contact> all);

}
