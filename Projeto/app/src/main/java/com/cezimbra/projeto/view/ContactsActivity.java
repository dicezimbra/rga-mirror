package com.cezimbra.projeto.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cezimbra.projeto.R;
import com.cezimbra.projeto.adapter.ContactsAdapter;
import com.cezimbra.projeto.controller.ApplicationComponent;
import com.cezimbra.projeto.interfaces.BackendInterface;
import com.cezimbra.projeto.interfaces.ContactBO;
import com.cezimbra.projeto.interfaces.RealmChangeInterface;
import com.cezimbra.projeto.model.Contact;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cezimbra on 31/08/2016.
 */
public class ContactsActivity extends BaseActivity implements Callback<List<Contact>>, RealmChangeInterface {

    @Inject
    ContactBO mContactBO;

    @Inject
    BackendInterface mBackend;

    @InjectView(R.id.rvList)
    RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView.Adapter mAdapter;

    ArrayList<Contact> mContactList;

    public static RealmChangeInterface mRealChangeInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        setupToolbar();
        injectViews();

        mContactList = new ArrayList<>();

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ContactsAdapter(this, mContactList);
        mRecyclerView.setAdapter(mAdapter);


        List<Contact> results = mContactBO.getContacts();

        if (isFirstTime()) {
            Call<List<Contact>> call = mBackend.getContacts();
            call.enqueue(this);
        } else {
            mContactList.addAll(results);
            mAdapter.notifyDataSetChanged();
        }

        mRealChangeInterface = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.fab)
    public void FabClick() {
        startActivity(new Intent(ContactsActivity.this, AddContactActivity.class));
    }

    @Override
    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {

        mContactList.clear();

        for (int i = 0; i < response.body().size(); i++) {
            Contact contact = response.body().get(i);
            contact.setId(i + 1);
            mContactList.add(contact);
        }

        mContactBO.addAll(response.body());
        setFirstTime(false);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onFailure(Call<List<Contact>> call, Throwable t) {
        t.printStackTrace();
    }

    @Override
    protected void performInjection(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onRealmChanges() {
        mContactList.clear();
        List<Contact> contacts = mContactBO.getContacts();
        mContactList.addAll(contacts);
        mAdapter.notifyDataSetChanged();
    }

    public boolean isFirstTime (){
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        boolean isFirstTime = sharedPref.getBoolean("firstTime", true);
        return isFirstTime;
    }

    public void setFirstTime(boolean firstTime){
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("firstTime", firstTime);
        editor.commit();
    }
}
