# README #

RGA PROJECT.

### Bibliotecas utilizadas ###


* butterknife - para melhorar a leitura e o entendimento do código
* retrofit2 and gson - para fazer as requisições e converter a resposta em objeto
* picasso - para trabalhar com o carregamento das imagens
* dagger - para injetar componentes singleton e melhorar a leitura e o entendimento do código

### Geração do APK ###

* Entre na pasta ~\Projeto
* Rode o seguinte comando:
	gradlew.bat assembleDebug
* Depois vá até a pasta ~\Projeto\app\build\outputs\apk e procure pelo arquivo app-debug.apk
* Instale-o em seu smartphone


### Explicações código e app ###

* No pacote view estão todas as activities.
* No pacote model estão os objetos usados na aplicação. 
* Na controller está o Application e a criação dos Singletons
* No pacote BO está a implementação do CRUD para os objetos

### Mudanças que eu faria se tivesse mais tempo ###

* Salvaria a imagem que o Picasso faz download
* Permitiria que o usuário, ao adicionar ou editar conseguisse também selecionar uma imagem
* Colocaria um listview infinito, que busca por mais itens quando chega ao final

